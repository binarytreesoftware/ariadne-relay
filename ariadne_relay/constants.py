"""Constants module"""
try:
    from django.db.models import QuerySet

    DJANGO_QUERYSET_AVAILABLE = True

except ModuleNotFoundError:
    DJANGO_QUERYSET_AVAILABLE = False

DEFAULT_PAGE_SIZE = 100
