"""Exceptions module"""


class AriadneRelayError(Exception):
    """Base Ariadne Relay exception"""

    def __init__(self, message=None):
        """Initializes the exception"""
        if message is None:
            message = self.default_message

        return super().__init__(message)


class IrretrievableNodeValueError(Exception):
    """Exception raised when a value from node cannot be retrieved"""

    default_message = (
        """The requested value cannot be retrieved from the supplied node"""
    )


class NotSettableNodeValueError(Exception):
    """Exception raised when a value from node cannot be set"""

    default_message = (
        """The supplied value cannot be set for the supplied node"""
    )


class InvalidNodeIdError(Exception):
    """Exception raised when a node id has invalid format"""

    default_message = "The node id is not valid"


class InvalidCursorError(Exception):
    """Exception raised when a cursor has invalid format"""

    default_message = "The cursor is not valid"
