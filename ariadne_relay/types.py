"""ariadne_relay types module"""

import os
from ariadne import InterfaceType
from ariadne import load_schema_from_path
from . import utils

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

definitions = load_schema_from_path(os.path.join(BASE_DIR, "schema"))

node_type = InterfaceType("Node")


@node_type.type_resolver
def resolve_relay_node_type(obj, *args):
    """Naive resolver for the node type"""
    return type(obj).__name__


@node_type.field("id")
def resolve_node_id(obj, *args):
    """Resolves the node id"""
    return utils.encode_node_id(obj)


__all__ = ["definitions", "node_type"]
