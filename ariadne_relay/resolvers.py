"""ariadne_relay resolvers module"""

from typing import Callable
from . import constants, utils

DJANGO_QUERYSET_AVAILABLE = constants.DJANGO_QUERYSET_AVAILABLE
DEFAULT_PAGE_SIZE = 100

if DJANGO_QUERYSET_AVAILABLE:
    QuerySet = constants.QuerySet


def create_node_resolver(collection, **kwargs) -> Callable:
    """Creates a node resolver"""
    id_field_name = kwargs.get("id_field_name", "id")

    def node_resolver(parent, info, id: str):
        """Resolves a node"""
        node_type, node_id = utils.decode_node_id(id)
        node = None

        if DJANGO_QUERYSET_AVAILABLE and isinstance(collection, QuerySet):
            filter_kwargs = {id_field_name: node_id}
            node = collection.filter(**filter_kwargs).first()

        else:
            for element in collection:
                is_subscriptable = hasattr(element, "__getitem__")
                allows_contains = hasattr(element, "__contains__")
                is_dict_like = is_subscriptable and allows_contains

                if is_dict_like and id_field_name in element:
                    value = str(element[id_field_name])

                    if value == node_id:
                        node = element
                        break

                elif hasattr(element, id_field_name):
                    value = str(getattr(element, id_field_name))

                    if value == node_id:
                        node = element
                        break

        return node

    return node_resolver


def generate_connection_data(
    nodes,
    base: int,
    has_previous_page: bool,
    has_next_page: bool,
    use_camel_case: bool = False,
):
    """Generates a standarized connection object"""

    connection_data = {
        "edges": [
            {
                "cursor": utils.encode_node_cursor(
                    node.__class__.__name__, base + position
                ),
                "node": node,
            }
            for position, node in enumerate(nodes)
        ]
    }

    class_name = nodes[0].__class__.__name__

    start_cursor = utils.encode_node_cursor(class_name, base)
    end_cursor = utils.encode_node_cursor(class_name, base + len(nodes) - 1)

    if use_camel_case:
        connection_data["pageInfo"] = {
            "hasNextPage": has_next_page,
            "hasPreviousPage": has_previous_page,
            "startCursor": start_cursor,
            "endCursor": end_cursor,
        }
    else:
        connection_data["page_info"] = {
            "has_next_page": has_next_page,
            "has_previous_page": has_previous_page,
            "start_cursor": start_cursor,
            "end_cursor": end_cursor,
        }

    return connection_data


def create_connection_resolver(collection, use_camel_case=False) -> Callable:
    """Creates a connection resolver"""

    def connection_resolver(
        parent, info, first: int = DEFAULT_PAGE_SIZE, after: str = None
    ):
        """Resolves a connection"""
        decoded_after = None

        if after is not None:
            object_type, decoded_after = utils.decode_node_cursor(after)

        if decoded_after is None:
            base = 0
        else:
            base = decoded_after + 1

        if DJANGO_QUERYSET_AVAILABLE and isinstance(collection, QuerySet):
            total_elements = collection.count()

        else:
            total_elements = len(collection)

        range_top = base + first
        sliced_elements = collection[base:range_top]

        # Make sure remaining_elements is never a negative number
        remaining_elements = max(0, total_elements - (range_top))

        has_next_page = remaining_elements > 0
        has_previous_page = base != 0

        return generate_connection_data(
            sliced_elements,
            base,
            has_previous_page,
            has_next_page,
            use_camel_case,
        )

    return connection_resolver
