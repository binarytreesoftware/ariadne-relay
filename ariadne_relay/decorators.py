"""Decorators module"""
from functools import wraps
from . import constants, utils


def node_resolver(wrapped):
    """Decorator for automatically decoding id"""

    @wraps(wrapped)
    def wrapper(parent, info, id, **kwargs):
        object_type, decoded_id = utils.decode_node_id(id)
        return wrapped(parent, info, decoded_id, **kwargs)

    return wrapper


def connection_resolver(wrapped, use_camel_case=False):
    """Decorator for automating connection resolution"""

    @wraps(wrapped)
    def wrapper(parent, info, **kwargs):
        first = kwargs.pop("first", constants.DEFAULT_PAGE_SIZE)
        after = kwargs.pop("after", -1)

        # Obtain possibly filtered nodes
        resolved_nodes = wrapped(parent, info, **kwargs)

        # Calculate the base position
        if isinstance(after, str):
            object_type, after = utils.decode_node_cursor(after)

        base = after + 1

        check_queryset = constants.DJANGO_QUERYSET_AVAILABLE is True

        # Check for Django QuerySets to prevent unnecesary retrieval
        if check_queryset and isinstance(resolved_nodes, constants.QuerySet):
            total_nodes = resolved_nodes.count()

        else:
            total_nodes = len(resolved_nodes)

        range_top = base + first
        paged_nodes = resolved_nodes[base:range_top]

        # Make sure remaining_elements is never a negative number
        remaining_elements = max(0, total_nodes - (range_top))

        has_next_page = remaining_elements > 0
        has_previous_page = base > 0

        object_type = paged_nodes[0].__class__.__name__

        start_cursor = utils.encode_node_cursor(object_type, base)
        end_cursor = utils.encode_node_cursor(
            object_type, base + len(paged_nodes) - 1
        )

        connection_data = {
            "edges": [
                {
                    "cursor": utils.encode_node_cursor(
                        node.__class__.__name__, base + position
                    ),
                    "node": node,
                }
                for position, node in enumerate(paged_nodes)
            ]
        }

        return {
            **connection_data,
            **utils.create_page_info_fragment(
                start_cursor, end_cursor, has_previous_page, has_next_page
            ),
        }

    return wrapper
