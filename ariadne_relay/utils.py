"""Utils module"""
import re
import base64
from . import exceptions

DEFAULT_NODE_ID_FIELD_NAME = "id"


def get_node_value(target, field_name):
    """Tries to get a value from a node"""
    is_subscriptable = hasattr(target, "__getitem__")
    allows_contains = hasattr(target, "__contains__")
    retrieval_success = False

    # Try with index based retrieval
    if is_subscriptable and allows_contains and field_name in target:
        value = target[field_name]
        retrieval_success = True

    # Try with attribute based retrieval
    if not retrieval_success and hasattr(target, field_name):
        value = getattr(target, field_name)
        retrieval_success = True

    # None of the above
    if not retrieval_success:
        raise exceptions.IrretrievableNodeValueError()

    return value


def set_node_value(target, field_name, value):
    """Tries to set a value from a node"""
    is_subscriptable = hasattr(target, "__setitem__")
    set_success = False

    # Try with index based setting
    if is_subscriptable:
        target[field_name] = value
        set_success = True

    # Try with attribute based setting
    if not set_success:
        try:
            value = setattr(target, field_name, value)
            set_success = True

        except AttributeError:
            pass

    # None of the above
    if not set_success:
        raise exceptions.NotSettableNodeValueError()


def encode_node_id(target, **kwargs) -> str:
    """Returns a globally unique id for an element based on its type and id"""
    field_name = kwargs.get("field_name", DEFAULT_NODE_ID_FIELD_NAME)
    type_name = kwargs.get("type_name", target.__class__.__name__)

    value = get_node_value(target, field_name)

    return base64.b64encode(
        ":".join([type_name, f"{value}"]).encode("utf-8")
    ).decode("utf-8")


def decode_node_id(node_id: str):
    """Decodes a globally unique id for an element"""

    try:
        decoded = base64.b64decode(node_id).decode("utf-8").split(":", 1)
        type_name, value = decoded

    except Exception:
        raise exceptions.InvalidNodeIdError(
            f"Got an invalid node id: '{node_id}'"
        )

    return type_name, value


def encode_node_cursor(object_type, position: int) -> str:
    """Returns a relay cursor for position"""
    return base64.b64encode(
        f"{object_type}:connection:{position}".encode("utf-8")
    ).decode("utf-8")


def decode_node_cursor(cursor: str) -> int:
    """Returns a relay cursor for position"""
    try:
        object_type, inner_type, position = (
            base64.b64decode(cursor.encode("utf-8")).decode("utf-8").split(":")
        )

    except Exception:
        raise exceptions.InvalidCursorError(f"Got an invalid cursor: {cursor}")

    return object_type, int(position)


def create_page_info_fragment(
    start_cursor, end_cursor, has_previous_page, has_next_page
):
    """Creates a dict with connection page information"""
    return {
        "page_info": {
            "has_next_page": has_next_page,
            "has_previous_page": has_previous_page,
            "start_cursor": start_cursor,
            "end_cursor": end_cursor,
        }
    }
