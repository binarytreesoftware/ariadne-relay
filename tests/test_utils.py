"""ariadne_relay utils testing module"""

import random
import unittest
from dataclasses import dataclass
from ariadne_relay import exceptions, utils


@dataclass
class Data:
    id: int
    name: str


class NodeValueRetrievalTestCase(unittest.TestCase):
    """Tests the node value retrieval functions"""

    def setUp(self):
        """Prepare for testing"""
        self.object_node = Data(random.randint(1000, 2000), "Name")
        self.dict_node = {
            utils.DEFAULT_NODE_ID_FIELD_NAME: random.randint(1000, 2000),
            "name": "Name",
        }

    def test_node_value_retrieval_for_objects_with_valid_attribute(self):
        """Tests the node retrieval for objects with valid attribute name"""
        self.assertEqual(
            utils.get_node_value(self.object_node, "id"), self.object_node.id
        )
        self.assertEqual(
            utils.get_node_value(self.object_node, "name"),
            self.object_node.name,
        )

    def test_node_value_retrieval_for_objects_with_invalid_attribute(self):
        """Tests the node retrieval for objects with invalid attribute name"""
        with self.assertRaises(exceptions.IrretrievableNodeValueError):
            self.assertEqual(
                utils.get_node_value(self.object_node, "_id"),
                self.object_node.id,
            )
            self.assertEqual(
                utils.get_node_value(self.object_node, "_name"),
                self.object_node.name,
            )

    def test_node_value_retrieval_dict_like_object_valid_attribute(self):
        """Tests the node retrieval for dicts with valid index name"""
        self.assertEqual(
            utils.get_node_value(self.dict_node, "id"), self.dict_node["id"]
        )
        self.assertEqual(
            utils.get_node_value(self.dict_node, "name"),
            self.dict_node["name"],
        )

    def test_node_value_retrieval_dict_like_object_invalid_attribute(self):
        """Tests the node retrieval for dicts with invalid index name"""
        with self.assertRaises(exceptions.IrretrievableNodeValueError):
            self.assertEqual(
                utils.get_node_value(self.dict_node, "_id"),
                self.dict_node["id"],
            )
            self.assertEqual(
                utils.get_node_value(self.dict_node, "_name"),
                self.dict_node["name"],
            )


class NodeValueSettingTestCase(unittest.TestCase):
    """Tests the node value retrieval functions"""

    def setUp(self):
        """Prepare for testing"""
        self.object_node = Data(random.randint(1000, 2000), "Name")
        self.dict_node = {
            utils.DEFAULT_NODE_ID_FIELD_NAME: random.randint(1000, 2000),
            "name": "Name",
        }

    def test_node_value_setting_for_objects_with_valid_attribute(self):
        """Tests the node retrieval for objects with valid attribute name"""
        value = random.randint(0, 10)
        utils.set_node_value(self.object_node, "id", value)
        self.assertEqual(self.object_node.id, value)

    def test_node_value_setting_for_objects_with_invalid_attribute(self):
        """Tests the node retrieval for objects with invalid attribute name"""
        with self.assertRaises(exceptions.NotSettableNodeValueError):
            node = object()
            utils.set_node_value(node, "id", random.randint(0, 10))

    def test_node_value_setting_dict_like_object(self):
        """Tests the node retrieval for dicts with valid index name"""
        value = random.randint(0, 10)
        utils.set_node_value(self.dict_node, "id", value)
        self.assertEqual(self.dict_node["id"], value)


class NodeIdGenerationTestCase(unittest.TestCase):
    """Tests for encoding and decoding of a global node id"""

    def setUp(self):
        """Prepare for testing"""
        self.object_node = Data(random.randint(1000, 2000), "Name")
        self.dict_node = {
            utils.DEFAULT_NODE_ID_FIELD_NAME: random.randint(1000, 2000),
            "name": "Name",
        }

    def test_encoding_for_objects_with_irritrievable_field_names(self):
        """Test encoding for objects with irretrievable field names"""
        with self.assertRaises(exceptions.IrretrievableNodeValueError):
            utils.encode_node_id(self.object_node, field_name="irretrievable")

    def test_encoding_for_objects_with_default_params(self):
        """Test encoding for objects with default params"""
        encoded_id = utils.encode_node_id(self.object_node)

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value,
            str(
                utils.get_node_value(
                    self.object_node, utils.DEFAULT_NODE_ID_FIELD_NAME
                )
            ),
        )

        self.assertEqual(type_name, self.object_node.__class__.__name__)

    def test_encoding_for_objects_with_custom_field_name(self):
        """Test encoding for objects with custom field name"""
        custom_field = "name"
        encoded_id = utils.encode_node_id(
            self.object_node, field_name=custom_field
        )

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value, str(utils.get_node_value(self.object_node, custom_field))
        )

    def test_encoding_for_objects_with_custom_type_name(self):
        """Test encoding for objects with custom type name"""
        custom_type = "CustomType"
        encoded_id = utils.encode_node_id(
            self.object_node, type_name=custom_type
        )

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value,
            str(
                utils.get_node_value(
                    self.object_node, utils.DEFAULT_NODE_ID_FIELD_NAME
                )
            ),
        )
        self.assertEqual(type_name, custom_type)

    def test_encoding_dict_like_object_irritrievable_field_names(self):
        """Test encoding for dict like object with irretrievable field names"""
        with self.assertRaises(exceptions.IrretrievableNodeValueError):
            utils.encode_node_id(self.dict_node, field_name="irretrievable")

    def test_encoding_dict_like_object_default_params(self):
        """Test encoding for dict like object with default params"""
        encoded_id = utils.encode_node_id(self.dict_node)

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value,
            str(
                utils.get_node_value(
                    self.dict_node, utils.DEFAULT_NODE_ID_FIELD_NAME
                )
            ),
        )

        self.assertEqual(type_name, self.dict_node.__class__.__name__)

    def test_encoding_dict_like_object_custom_field_name(self):
        """Test encoding for dict like object with custom field name"""
        custom_field = "name"
        encoded_id = utils.encode_node_id(
            self.dict_node, field_name=custom_field
        )

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value, str(utils.get_node_value(self.dict_node, custom_field))
        )

    def test_encoding_dict_like_object_custom_type_name(self):
        """Test encoding for dict like object with custom type name"""
        custom_type = "CustomType"
        encoded_id = utils.encode_node_id(
            self.dict_node, type_name=custom_type
        )

        self.assertIsNotNone(encoded_id)
        self.assertIsInstance(encoded_id, str)

        type_name, value = utils.decode_node_id(encoded_id)
        self.assertEqual(
            value,
            str(
                utils.get_node_value(
                    self.dict_node, utils.DEFAULT_NODE_ID_FIELD_NAME
                )
            ),
        )
        self.assertEqual(type_name, custom_type)

    def test_decoding_invalid_ids(self):
        """Tests decoding of valid ids"""
        invalid_ids = ["InVaLiD", None, 1]

        for current in invalid_ids:
            with self.subTest(current=current):
                with self.assertRaises(exceptions.InvalidNodeIdError):
                    utils.decode_node_id(current)


class CursorGenerationTestCase(unittest.TestCase):
    """Tests for encoding and decoding of connection cursors"""

    def test_cursor_generation(self):
        """Tests that the cursors are being generated correctly"""
        for current in range(0, 5):
            expected_type = "some_type"
            expected_position = random.randint(0, 1_000_000)
            subtest_kwargs = {
                "position": expected_position,
                "type": expected_type,
            }

            with self.subTest(**subtest_kwargs):
                encoded_cursor = utils.encode_node_cursor(
                    expected_type, expected_position
                )

                self.assertIsNotNone(encoded_cursor)
                self.assertIsInstance(encoded_cursor, str)

                object_type, position = utils.decode_node_cursor(
                    encoded_cursor
                )

                self.assertEqual(object_type, expected_type)
                self.assertEqual(position, expected_position)

    def test_invalid_cursor_decoding(self):
        """Tests that invalid cursors raise exceptions"""
        invalid_cursors = ["InVaLiD", None, 1]

        for current in invalid_cursors:
            with self.subTest(current=current):
                with self.assertRaises(exceptions.InvalidCursorError):
                    utils.decode_node_cursor(current)
