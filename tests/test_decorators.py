"""ariadne_relay resolver testing module"""
import math
import unittest
from ariadne_relay import decorators, utils
from data import create_collections


class NodeResolverDecoratorTestCase(unittest.TestCase):
    """Tests node resolver decorator functionality"""

    def setUp(self):
        """Prepares the for the tests"""
        self.collections = create_collections()

    def test_node_data_retrieval(self):
        """Tests the retrieval of nodes"""

        for collection in self.collections:
            collection_type = collection.__class__.__name__

            @decorators.node_resolver
            def resolver(parent, info, id, **kwargs):
                return list(
                    filter(
                        lambda element: (
                            str(utils.get_node_value(element, "id")) == id
                        ),
                        parent,
                    )
                )[0]

            for element in collection:
                subtest_kwargs = {"type": collection_type, "element": element}

                with self.subTest(subtest_kwargs):
                    node_id = utils.encode_node_id(element)
                    result = resolver(collection, object(), node_id)

                    self.assertIsNotNone(result)

                    is_dict_like = hasattr(element, "__getitem__") and hasattr(
                        element, "__contains__"
                    )

                    if is_dict_like:
                        self.assertIn("id", result)

                    else:
                        self.assertTrue(hasattr(result, "id"))


class CollectionResolverDecoratorTestCase(unittest.TestCase):
    """Tests collection resolver decorator functionality"""

    def setUp(self):
        """Prepares the for the tests"""
        self.collections = create_collections()

    def test_connection_data_retrieval(self):
        """Tests the generation of connection data for lists of objects"""

        for collection in self.collections:
            collection_type = collection.__class__.__name__

            total_elements = len(collection)
            page_size = int(total_elements / 10 * 4)
            pages = math.ceil(total_elements / page_size)

            @decorators.connection_resolver
            def resolve(parent, info, **kwargs):
                return collection

            remaining_elements = total_elements
            last_end_cursor = None

            for page in range(0, pages):
                is_first_page = page == 0
                is_last_page = page == (pages - 1)

                subtest_kwargs = {
                    "type": collection_type,
                    "page": page,
                    "is_first": is_first_page,
                    "is_last": is_last_page,
                }

                with self.subTest(**subtest_kwargs):
                    kwargs = {"first": page_size}

                    if last_end_cursor is not None:
                        kwargs["after"] = last_end_cursor

                    result = resolve(None, object(), **kwargs)
                    result_count = len(result["edges"])

                    # Check the results against page size
                    if not is_last_page:
                        self.assertEqual(result_count, page_size)
                        remaining_elements -= page_size

                    else:
                        self.assertEqual(result_count, remaining_elements)

                    # Check pages
                    if is_first_page:
                        self.assertFalse(
                            result["page_info"]["has_previous_page"]
                        )

                    elif is_last_page:
                        self.assertFalse(result["page_info"]["has_next_page"])

                    else:
                        self.assertTrue(
                            result["page_info"]["has_previous_page"]
                        )
                        self.assertTrue(result["page_info"]["has_next_page"])

                    # Check cursors
                    base_index = page * page_size

                    first_element = collection[base_index]
                    first_node = result["edges"][0]["node"]

                    try:
                        first_element_id = first_element["id"]
                        first_node_id = first_node["id"]

                    except Exception:
                        first_element_id = first_element.id
                        first_node_id = first_node.id

                    self.assertEqual(first_element_id, first_node_id)
                    self.assertEqual(
                        result["page_info"]["start_cursor"],
                        utils.encode_node_cursor(
                            first_element.__class__.__name__, base_index
                        ),
                    )

                    if not is_last_page:
                        last_position = base_index + page_size - 1
                        last_element = collection[last_position]

                    else:
                        last_position = len(collection) - 1
                        last_element = collection[last_position]

                    last_node = result["edges"][-1]["node"]

                    try:
                        last_element_id = last_element["id"]
                        last_node_id = last_node["id"]

                    except Exception:
                        last_element_id = last_element.id
                        last_node_id = last_node.id

                    self.assertEqual(last_element_id, last_node_id)
                    self.assertEqual(
                        result["page_info"]["end_cursor"],
                        utils.encode_node_cursor(
                            last_element.__class__.__name__, last_position
                        ),
                    )

                    last_end_cursor = result["page_info"]["end_cursor"]
