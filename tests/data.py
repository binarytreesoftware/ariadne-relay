"""ariadne_relay test data module"""
from dataclasses import dataclass


@dataclass
class Entity:
    id: int
    name: str


PRIMES = [1, 3, 5, 7, 11, 13, 17, 19]


def create_dict_collection():
    """Creates a dict collection"""
    return [
        {"id": prime, "name": f"Entity {prime}"} for prime in reversed(PRIMES)
    ]


def create_data_class_collection():
    """Creates a data class collection"""
    return [Entity(prime, f"Name {prime}") for prime in reversed(PRIMES)]


def create_collections():
    """Creates collections for testing"""
    return [create_data_class_collection(), create_dict_collection()]
