"""ariadne_relay type testing module"""
import os
import ariadne
import unittest
from ariadne_relay import resolvers, types, utils
from unittest.mock import Mock

from data import create_data_class_collection, Entity

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

type_definitions = ariadne.load_schema_from_path(
    os.path.join(BASE_DIR, "schema.graphql")
)


class SingleNodeTestCase(unittest.TestCase):
    """Tests for encoding and decoding of a global node id"""

    def setUp(self):
        """Prepare the data for tests"""
        self.collection = create_data_class_collection()

        self.query_one_resolver = Mock(
            wraps=resolvers.create_node_resolver(self.collection)
        )

        query_type = ariadne.QueryType()
        query_type.set_field("entity", self.query_one_resolver)

        definitions = [type_definitions, types.definitions]

        bindables = [query_type, types.node_type]

        self.schema = ariadne.make_executable_schema(definitions, bindables)

    def test_single_node_retrieval(self):
        """Tests the retrieval of a single node"""

        for entity in self.collection:
            success, result = ariadne.graphql_sync(
                self.schema,
                {
                    "query": """
                        query getEntity($id: ID!) {
                            entity(id: $id) {
                                id
                                name
                            }
                        }""",
                    "variables": {"id": utils.encode_node_id(entity)},
                },
            )

            self.assertIn("data", result)
            data = result["data"]

            self.assertIn("entity", data)
            retrieved_entity = data["entity"]

            self.assertIsNotNone(retrieved_entity)
            self.assertIn("id", retrieved_entity)
            self.assertIn("name", retrieved_entity)

            node_type, node_id = utils.decode_node_id(retrieved_entity["id"])
            self.assertEqual(node_id, str(entity.id))


class ConnectionTestCaseMixin(object):
    """Mixin for provinding data for connection tests"""

    def setUp(self):
        """Prepare the data for tests"""
        self.collection = create_data_class_collection()

        self.query_all_resolver = Mock(
            wraps=resolvers.create_connection_resolver(
                self.collection, use_camel_case=self.use_camel_case
            )
        )

        query_type = ariadne.QueryType()
        query_type.set_field("allEntities", self.query_all_resolver)

        definitions = [type_definitions, types.definitions]

        bindables = [query_type, types.node_type]

        if not self.use_camel_case:
            bindables.append(ariadne.snake_case_fallback_resolvers)

        self.schema = ariadne.make_executable_schema(definitions, bindables)

        self.query = """
                query getAllEntities {
                    allEntities {
                        pageInfo {
                            hasNextPage
                            hasPreviousPage
                            startCursor
                            endCursor
                        }
                        edges {
                            cursor
                            node {
                                id
                                name
                            }
                        }
                    }
                }"""

    def test_edges_from_connection_retrieval(self):
        """Tests the retrieval of data from a connection"""

        success, result = ariadne.graphql_sync(
            self.schema, {"query": self.query}
        )

        self.assertTrue(self.query_all_resolver.called)

        self.assertIn("data", result)
        data = result["data"]

        self.assertIn("allEntities", data)
        self.assertIsNotNone(data["allEntities"])

        self.assertIn("pageInfo", data["allEntities"])
        page_info = data["allEntities"]["pageInfo"]

        self.assertIn("hasNextPage", page_info)
        self.assertIn("hasPreviousPage", page_info)
        self.assertIn("startCursor", page_info)
        self.assertIn("endCursor", page_info)

        self.assertIn("edges", data["allEntities"])
        edges = data["allEntities"]["edges"]

        for edge in edges:
            self.assertIn("cursor", edge)
            self.assertIn("node", edge)
            self.assertIn("id", edge["node"])
            self.assertIn("name", edge["node"])

            type_name, value = utils.decode_node_id(edge["node"]["id"])

            self.assertIsNotNone(type_name)
            self.assertEqual(type_name, Entity.__name__)


class CamelCaseConnectionTestCase(ConnectionTestCaseMixin, unittest.TestCase):
    """Tests for encoding and decoding of a global node id"""

    use_camel_case = True


class SnakeCaseConnectionTestCase(ConnectionTestCaseMixin, unittest.TestCase):
    """Tests for encoding and decoding of a global node id"""

    use_camel_case = False
